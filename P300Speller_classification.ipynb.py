
import itertools
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
from sklearn.decomposition import FastICA
from sklearn.decomposition import PCA
from sklearn.metrics import (classification_report, confusion_matrix, accuracy_score,
                             brier_score_loss, precision_score, recall_score, f1_score, balanced_accuracy_score)
from sklearn.covariance import EmpiricalCovariance
from sklearn.svm import LinearSVC, SVC
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.ensemble import VotingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier, AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, FunctionTransformer
import sklearn
from wyrm import processing as proc
from wyrm.types import Data
from wyrm import plot
import matplotlib.colors

import pandas as pd
import numpy as np
import scipy.io as spio
from matplotlib import pyplot as plt
from matplotlib import ticker
import matplotlib as mpl
mpl.font_manager._rebuild()  # fix bug matplotlib cannot find cached Fonts,
# logging.getLogger('matplotlib.font_manager').disabled = True

get_ipython().run_line_magic('matplotlib', 'inline')
sns.set_style("whitegrid")


ELOC = 'data/BCI_Comp_III_Wads_2004/eloc64.txt'

TRAIN_A = 'data/BCI_Comp_III_Wads_2004/Subject_A_Train.mat'
TRAIN_B = 'data/BCI_Comp_III_Wads_2004/Subject_B_Train.mat'

TEST_A = 'data/BCI_Comp_III_Wads_2004/Subject_A_Test.mat'
TEST_B = 'data/BCI_Comp_III_Wads_2004/Subject_B_Test.mat'

TRUE_LABELS_A = 'WQXPLZCOMRKO97YFZDEZ1DPI9NNVGRQDJCUVRMEUOOOJD2UFYPOO6J7LDGYEGOA5VHNEHBTXOO1TDOILUEE5BFAEEXAW_K4R3MRU'
TRUE_LABELS_B = 'MERMIROOMUHJPXJOHUVLEORZP3GLOO7AUFDKEFTWEOOALZOP9ROCGZET1Y19EWX65QUYU7NAK_4YCJDVDNGQXODBEV2B5EFDIDNR'

MATRIX = ['abcdef',
          'ghijkl',
          'mnopqr',
          'stuvwx',
          'yz1234',
          '56789_']

# Features we care about
MARKER_DEF_TRAIN = {'target': ['target'], 'nontarget': ['nontarget']}
MARKER_DEF_TEST = {'flashing': ['flashing']}

N_INTENSIFY = 15  # 15 intensifications of row/column
N_STIMULI = 12  # 12 stimuli of each character

SEG_IVAL = [0, 700]  # The interval in milliseconds to cut around the markers

# CHANNELS = ["FCz", "C3", "Cz", "C4", "CP1", "CPz", "CP2", "Pz"]


def load_mat_data(filename):
    STIMULUS_CODE = {
        # cols from left to right
        1: "agmsy5",
        2: "bhntz6",
        3: "ciou17",
        4: "djpv28",
        5: "ekqw39",
        6: "flrx4_",
        # rows from top to bottom
        7: "abcdef",
        8: "ghijkl",
        9: "mnopqr",
        10: "stuvwx",
        11: "yz1234",
        12: "56789_"
    }

    # load the matlab data
    data_mat = spio.loadmat(filename)
    # load the channel names (the same for all datasets
    eloc_file = ELOC
    with open(eloc_file) as fh:
        data = fh.read()
    channels = []
    for line in data.splitlines():
        if line:
            chan = line.split()[-1]
            chan = chan.replace('.', '')
            channels.append(chan)
    # fix the channel names, some letters have the wrong capitalization
    for i, s in enumerate(channels):
        s2 = s.upper()
        s2 = s2.replace('Z', 'z')
        s2 = s2.replace('FP', 'Fp')
        channels[i] = s2
    # The signal is recorded with 64 channels, bandpass filtered
    # 0.1-60Hz and digitized at 240Hz. The format is Character Epoch x
    # Samples x Channels
    data = data_mat['Signal']
    n_channels = len(channels)
    data = data.astype('double')

    # For each sample: 1 if a row/colum was flashed, 0 otherwise
    flashing = data_mat['Flashing'].reshape(-1)
    #flashing = np.flatnonzero((np.diff(a) == 1)) + 1
    tmp = []
    for i, _ in enumerate(flashing):
        if i == 0:
            tmp.append(flashing[i])
            continue
        if flashing[i] == flashing[i-1] == 1:
            tmp.append(0)
            continue
        tmp.append(flashing[i])
    flashing = np.array(tmp)
    # For each sample: 0 when no row/colum was intensified,
    # 1..6 for intensified columns, 7..12 for intensified rows
    stimulus_code = data_mat['StimulusCode'].reshape(-1)
    stimulus_code = stimulus_code[flashing == 1]
    # 0 if no row/col was intensified or the intensified did not contain
    # the target character, 1 otherwise
    stimulus_type = data_mat.get('StimulusType', np.array([])).reshape(-1)
    # The target characters
    target_chars = data_mat.get('TargetChar', np.array([])).reshape(-1)
    fs = 240  # Frequency samples
    data = data.reshape(-1, n_channels)
    timeaxis = np.linspace(
        0, data.shape[0] / fs * 1000, data.shape[0], endpoint=False)
    dat = Data(data=data, axes=[timeaxis, channels], names=[
               'time', 'channel'], units=['ms', '#'])
    dat.fs = fs
    # preparing the markers
    target_mask = np.logical_and((flashing == 1), (stimulus_type == 1)) if len(
        stimulus_type) > 0 else []
    nontarget_mask = np.logical_and(
        (flashing == 1), (stimulus_type == 0)) if len(stimulus_type) > 0 else []
    flashing = (flashing == 1)
    flashing = [[i, 'flashing'] for i in timeaxis[flashing]]
    targets = [[i, 'target'] for i in timeaxis[target_mask]]
    nontargets = [[i, 'nontarget'] for i in timeaxis[nontarget_mask]]
    dat.stimulus_code = stimulus_code[:]
    stimulus_code = zip([t for t, _ in flashing], [
                        STIMULUS_CODE[i] for i in stimulus_code])
    markers = flashing[:]
    markers.extend(targets)
    markers.extend(nontargets)
    markers.extend(stimulus_code)
    dat.markers = sorted(markers[:], key=lambda x: x[0])
    dat.target_chars = target_chars
    return dat


def standardise_data(feature_vector, scaler, is_training):

    X = feature_vector.data
    y = feature_vector.axes[0]

    # Fit on training set only
    if is_training:
        scaler.fit(X)

    X = scaler.transform(X)
    # update value of pre-processed X
    feature_vector.data = X

    return feature_vector


def pca_apply(pca, feature_vector, is_training):

    X = feature_vector.data
    y = feature_vector.axes[0]

    print('pca_clean transform data isTraining {} shape BEFORE {}'.format(
        is_training, X.shape))

    if is_training:
        pca.fit(X)

    X = pca.transform(X)
    # update value of pre-processed X
    feature_vector.data = X
    print('pca_clean transform data isTraining {} shape AFTER {}'.format(
        is_training, X.shape))

    return feature_vector


def ica_apply(ica, feature_vector, is_training):

    X = feature_vector.data
    y = feature_vector.axes[0]

    print('ICA transform data isTraining {} shape BEFORE {}'.format(
        is_training, X.shape))

    if is_training:
        ica.fit(X)

    X = ica.transform(X)
    # update value of pre-processed X
    feature_vector.data = X
    print('ICA transform data isTraining {} shape AFTER {}'.format(
        is_training, (X.shape)))

    return feature_vector


def preprocessing_simple(dat, ica, pca, scaler, is_training, MRK_DEF, *args, **kwargs):
    fs_n = dat.fs / 2
    b, a = proc.signal.butter(
        5, [30 / fs_n], btype='low')  # filter low-pass 30Hz
    dat = proc.filtfilt(dat, b, a)

    # Subsample the data to 20 Hz.
    dat = proc.subsample(dat, 20)

    # Convert a continuous data object to an epoched one.
    epo = proc.segment_dat(dat, MRK_DEF, SEG_IVAL)

    feature_vector = proc.create_feature_vectors(epo)

    if scaler is not None:
        feature_vector = standardise_data(feature_vector, scaler, is_training)

    if pca is not None:
        feature_vector = pca_apply(pca, feature_vector, is_training)

    if ica is not None:
        feature_vector = ica_apply(ica, feature_vector, is_training)

    return feature_vector, epo


# ## Method to process prediction
def process_prediction(fv, y_pred, nrepetition, true_label):
    label_len = len(true_label)

    # unscramble the order of stimuli
    unscramble_idx = fv.stimulus_code.reshape(
        label_len, N_INTENSIFY, N_STIMULI).argsort()
    static_idx = np.indices(unscramble_idx.shape)
    y_pred = y_pred.reshape(label_len, N_INTENSIFY, N_STIMULI)
    y_pred = y_pred[static_idx[0], static_idx[1], unscramble_idx]

    y_pred = y_pred[:, :nrepetition, :]

    # destil the result of the nrepetition runs
    y_pred = y_pred.sum(axis=1)

    y_pred = y_pred.argsort()

    cols = y_pred[y_pred <= 5].reshape(label_len, -1)
    rows = y_pred[y_pred > 5].reshape(label_len, -1)
    text = ''
    for i in range(label_len):
        row = rows[i][-1]-6
        col = cols[i][-1]
        letter = MATRIX[row][col]
        text += letter
    print
    print('---Constructed labels: %s' % text.upper())
    print('---True labels       : %s' % true_label)
    a = np.array(list(text.upper()))
    b = np.array(list(true_label))
    accuracy = np.count_nonzero(a == b) / len(a)
    print('---Accuracy: %.1f%%' % (accuracy * 100))
    return accuracy


# # pre-process data
training_set = [TRAIN_A, TRAIN_B]
testing_set = [TEST_A, TEST_B]
labels = [TRUE_LABELS_A, TRUE_LABELS_B]

dat_training = [load_mat_data(training_set[0]), load_mat_data(training_set[1])]
dat_test = [load_mat_data(testing_set[0]), load_mat_data(testing_set[1])]

# # select channels we want
# CHANNELS = ['Cz']
CHANNELS = ["FCz", "C3", "Cz", "C4", "CP1", "CPz", "CP2", "Pz"]
dat_training = [proc.select_channels(
    dat_training[0], CHANNELS), proc.select_channels(dat_training[0], CHANNELS)]
dat_test = [proc.select_channels(
    dat_test[0], CHANNELS), proc.select_channels(dat_test[0], CHANNELS)]

scaler = [StandardScaler(), StandardScaler()]
# scaler = [None, None]

# choose the minimum number of principal components such that 95% of the variance is retained.
pca = [PCA(0.95), PCA(0.95)]
# pca = [None, None] #choose the minimum number of principal components such that 95% of the variance is retained.
# pca = [PCA(n_components=10), PCA(n_components=10)]

# ica = [FastICA(n_components=100,max_iter=1000, tol=0.005), FastICA(n_components=100,max_iter=1000, tol=0.005)]
ica = [None, None]

fv_epo_train = [preprocessing_simple(dat_training[0], ica[0], pca[0], scaler[0], True, MARKER_DEF_TRAIN), preprocessing_simple(
    dat_training[1], ica[1], pca[1], scaler[1], True, MARKER_DEF_TRAIN)]
fv_epo_test = [preprocessing_simple(dat_test[0], ica[0], pca[0], scaler[0], False, MARKER_DEF_TEST), preprocessing_simple(
    dat_test[1], ica[1], pca[1], scaler[1], False, MARKER_DEF_TEST)]

fv_train = [x[0] for x in fv_epo_train]
epo_train = [x[1] for x in fv_epo_train]

fv_test = [x[0] for x in fv_epo_test]
epo_test = [x[1] for x in fv_epo_test]


lda_covs = [None, None]  # classifiers

for subject in range(2):
    # train the lda with Covarience https://scikit-learn.org/stable/modules/covariance.html
    cfy = proc.lda_train(fv_train[subject])

    print('Training done for subject {}'.format(subject))

    # add to list of classifiers
    lda_covs[subject] = cfy


# ## List of classifiers

# Define list of classifiers
classifiers = [
    [
        GradientBoostingClassifier(learning_rate=0.1),
        AdaBoostClassifier(n_estimators=100, random_state=0),
        KNeighborsClassifier(3),
        DecisionTreeClassifier(),
        RandomForestClassifier(n_estimators=100),
        LinearDiscriminantAnalysis(),
        QuadraticDiscriminantAnalysis(),
        LinearSVC(max_iter=10000),
        SVC(C=10, gamma='auto'),
    ],
    [
        GradientBoostingClassifier(learning_rate=0.1),
        AdaBoostClassifier(n_estimators=100, random_state=0),
        KNeighborsClassifier(3),
        DecisionTreeClassifier(),
        RandomForestClassifier(n_estimators=100),
        LinearDiscriminantAnalysis(),
        QuadraticDiscriminantAnalysis(),
        LinearSVC(max_iter=10000),
        SVC(C=10, gamma='auto')
    ]
]


# ### Train data

# split data
log_cols = ['Accuracy', 'Classifier']

log_score_trainings = []

for subject in range(2):
    print("Start Subject: {}".format(subject))
    fv = fv_train[subject]
    X = fv.data
    y = fv.axes[0]
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42)

    log_score_training = pd.DataFrame(columns=log_cols)

    for clf in classifiers[subject]:
        name = clf.__class__.__name__

        if name == "GradientBoostingClassifier":
            name = "GradientBoostingClassifier" + "_" + str(clf.learning_rate)

        if name == "LinearDiscriminantAnalysis":
            name = "LinearDiscriminantAnalysis" + "_" + \
                clf.solver + "_Shrinkage:" + str(clf.shrinkage)

        print("== Start Classifier: {}".format(name))
        clf.fit(X_train, y_train)

        y_pred = clf.predict(X_test)
        score = accuracy_score(y_test, y_pred)

        cm = confusion_matrix(y_test, y_pred)
        print(cm)

        TN = cm[1][1]
        FN = cm[0][1]
        TP = cm[0][0]
        FP = cm[1][0]

        print('TN: {}, FN: {}, TP: {}, FP: {}'.format(TN, FN, TP, FP))

        # Sensitivity, hit rate, recall, or true positive rate
        TPR = TP/(TP+FN)
        # Specificity or true negative rate
        TNR = TN/(TN+FP)
        # Precision or positive predictive value
        PPV = TP/(TP+FP)
        # Negative predictive value
        NPV = TN/(TN+FN)
        # Fall out or false positive rate
        FPR = FP/(FP+TN)
        # False negative rate
        FNR = FN/(TP+FN)
        # False discovery rate
        FDR = FP/(TP+FP)

        # Overall accuracy
        ACC = (TP+TN)/(TP+FP+FN+TN)
        print('\tAccuracy     : {:.2f}'.format(ACC))
        print('\tPrecision    : {:.2f}'.format(PPV))
        print('\tRecall       : {:.2f}'.format(TPR))
        F1 = 2*(PPV*TPR)/(PPV+TPR)
        print('\tF1           : {:.2f}'.format(F1))
        BM = TPR + TNR - 1
        print('\tInformedness : {:.2f}'.format(BM))

        print("== End Classifier: {}".format(name))
        print("== -------------")

        log_entry = pd.DataFrame([[name, score*100]], columns=log_cols)
        log_score_training = log_score_training.append(log_entry, sort=False)

    log_score_trainings.append(log_score_training)


# ## Use trained classifiers on TRAINING dataset

log_trainings = []

cols_repetitions = ["Repetition", "Accuracy", "ClfName"]

for subject in range(2):
    print("==== Start Subject: {}".format(subject))
    log_training_repeats = {}
    labels_train = dat_training[subject].target_chars[0]

    for clf in classifiers[subject]:
        name = clf.__class__.__name__

        if name == "GradientBoostingClassifier":
            name = "GradientBoostingClassifier" + "_" + str(clf.learning_rate)

        if name == "LinearDiscriminantAnalysis":
            name = "LDA_scikitlearn"

        print("==== Start classifier: {}".format(name))
        y_pred = clf.predict(fv_train[subject].data)

        log_training_repeat = pd.DataFrame(columns=cols_repetitions)
        for repeat_idx in range(1, N_INTENSIFY + 1):
            print("-- Repetition: {}".format(repeat_idx))
            name_repeat = name + "-" + str(repeat_idx)
            score = process_prediction(
                fv_train[subject], y_pred, repeat_idx, labels_train)
            log_entry = pd.DataFrame(
                [[repeat_idx, score*100, name]], columns=cols_repetitions)
            log_training_repeat = log_training_repeat.append(
                log_entry, sort=False)

        log_training_repeats[name] = log_training_repeat

        print("==== End classifier: {}".format(name))

    # append LDA cov
    name = 'LDA_wyrm'
    # add LDA cov estimator
    print("==== Start classifier: {}".format(name))
    y_pred = proc.lda_apply(fv_train[subject], lda_covs[subject])
    log_training_repeat = pd.DataFrame(columns=cols_repetitions)
    for repeat_idx in range(1, N_INTENSIFY + 1):
        print("-- Repetition: {}".format(repeat_idx))
        name_repeat = name + "-" + str(repeat_idx)
        score = process_prediction(
            fv_train[subject], y_pred, repeat_idx, labels_train)
        log_entry = pd.DataFrame(
            [[repeat_idx, score*100, name]], columns=cols_repetitions)
        log_training_repeat = log_training_repeat.append(log_entry, sort=False)

    log_training_repeats[name] = log_training_repeat

    log_trainings.append(log_training_repeats)


# ### Plot results of each repetitions for subject A


sns.set_style("whitegrid")
plt.figure(figsize=(8, 6))

marker = itertools.cycle(
    ('o', 'v', '^', '<', '>', 's', '8', 'p', 'X', 'D', "*"))

ax = plt.gca()
ax.set_prop_cycle(None)  # if matplotlib <1.5 use set_color_cycle

log_training_repeats = log_trainings[0]
for clf_name in log_training_repeats:
    log_training_repeat = log_training_repeats.get(clf_name)

    color = next(ax._get_lines.prop_cycler)['color']
    plt.plot('Repetition', 'Accuracy', data=log_training_repeat, label=clf_name,
             linestyle='dashed', markeredgecolor='none',  marker=next(marker), color=color, markersize=12)

# Put the legend out of the figure
plt.legend(bbox_to_anchor=(1.05, 1), loc=2,
           borderaxespad=0., prop={'size': 14})
plt.xticks(ticks=list(range(1, 16)))
plt.xlabel('Repetitions')
plt.ylabel('Accuracy %')
plt.title('Classifier Accuracy TRAINING subject A')
plt.show()


# ### Plot results of each repetitions for subject B


sns.set_style("whitegrid")
plt.figure(figsize=(8, 6))

ax = plt.gca()
ax.set_prop_cycle(None)  # if matplotlib <1.5 use set_color_cycle

log_training_repeats = log_trainings[1]
for clf_name in log_training_repeats:
    log_training_repeat = log_training_repeats.get(clf_name)
    color = next(ax._get_lines.prop_cycler)['color']
    plt.plot('Repetition', 'Accuracy', data=log_training_repeat, label=clf_name,
             linestyle='dashed', markeredgecolor='none',  marker=next(marker), color=color, markersize=12)

# Put the legend out of the figure
plt.legend(bbox_to_anchor=(1.05, 1), loc=2,
           borderaxespad=0., prop={'size': 14})
plt.xticks(ticks=list(range(1, 16)))
plt.xlabel('Repetitions')
plt.ylabel('Accuracy %')
plt.title('Classifier Accuracy TRAINING subject B')
plt.show()


# In[ ]:


for subject in range(2):
    df_list = []
    log_training_repeats = log_trainings[subject]
    for clf_name in log_training_repeats:
        df_transpose = log_training_repeats.get(clf_name).T
        df_list.append(df_transpose[1:2])

    dt_csv = pd.concat(df_list)
    dt_csv.to_csv(r'8Channels_NoPCA_Training_filter_change_table_Subject{}.csv'.format(
        'AB'[subject]), index=False, header=False)


# ## Apply classifiers on Test dataset for each repetition

# In[ ]:


log_testings = []

for subject in range(2):
    print("==== Start Subject: {}".format(subject))
    log_repeats = {}

    for clf in classifiers[subject]:
        name = clf.__class__.__name__

        if name == "GradientBoostingClassifier":
            name = "GradientBoostingClassifier" + "_" + str(clf.learning_rate)

        if name == "LinearDiscriminantAnalysis":
            name = "LDA"

        print("==== Start classifier: {}".format(name))
        y_pred = clf.predict(fv_test[subject].data)

        log_repeat = pd.DataFrame(columns=cols_repetitions)
        for repeat_idx in range(1, N_INTENSIFY + 1):
            print("-- Repetition: {}".format(repeat_idx))
            name_repeat = name + "-" + str(repeat_idx)
            score = process_prediction(
                fv_test[subject], y_pred, repeat_idx, labels[subject])
            log_entry = pd.DataFrame(
                [[repeat_idx, score*100, name]], columns=cols_repetitions)
            log_repeat = log_repeat.append(log_entry, sort=False)

        log_repeats[name] = log_repeat

        print("==== End classifier: {}".format(name))

    # append LDA cov
    name = 'LDA_wyrm'
    # add LDA cov estimator
    print("==== Start classifier: {}".format(name))
    y_pred = proc.lda_apply(fv_test[subject], lda_covs[subject])
    log_repeat = pd.DataFrame(columns=cols_repetitions)
    for repeat_idx in range(1, N_INTENSIFY + 1):
        print("-- Repetition: {}".format(repeat_idx))
        name_repeat = name + "-" + str(repeat_idx)
        score = process_prediction(
            fv_test[subject], y_pred, repeat_idx, labels[subject])
        log_entry = pd.DataFrame(
            [[repeat_idx, score*100, name]], columns=cols_repetitions)
        log_repeat = log_repeat.append(log_entry, sort=False)

    log_repeats[name] = log_repeat

    print("==== End classifier: {}".format(name))
    log_testings.append(log_repeats)


# In[ ]:


sns.set_style("whitegrid")
plt.figure(figsize=(8, 6))

# multiple line plot
ax = plt.gca()
ax.set_prop_cycle(None)  # if matplotlib <1.5 use set_color_cycle

log_repeats = log_testings[0]
for clf_name in log_repeats:
    log_repeat = log_repeats.get(clf_name)
    color = next(ax._get_lines.prop_cycler)['color']
    plt.plot('Repetition', 'Accuracy', data=log_repeat, label=clf_name, linestyle='dashed',
             markeredgecolor='none',  marker=next(marker), color=color, markersize=12)


# Put the legend out of the figure
plt.legend(bbox_to_anchor=(1.05, 1), loc=2,
           borderaxespad=0., prop={'size': 14})
plt.xticks(ticks=list(range(1, 16)))
plt.xlabel('Repetitions')
plt.ylabel('Accuracy %')
plt.title('Classifier Accuracy TESTING subject A')
plt.show()


# In[ ]:


# In[ ]:


sns.set_style("whitegrid")
plt.figure(figsize=(8, 6))

# multiple line plot
ax = plt.gca()
ax.set_prop_cycle(None)  # if matplotlib <1.5 use set_color_cycle

log_repeats = log_testings[1]
for clf_name in log_repeats:
    log_repeat = log_repeats.get(clf_name)
    color = next(ax._get_lines.prop_cycler)['color']
    plt.plot('Repetition', 'Accuracy', data=log_repeat, label=clf_name, linestyle='dashed',
             markeredgecolor='none',  marker=next(marker), color=color, markersize=12)

# Put the legend out of the figure
plt.legend(bbox_to_anchor=(1.05, 1), loc=2,
           borderaxespad=0., prop={'size': 14})
plt.xticks(ticks=list(range(1, 16)))
plt.xlabel('Repetitions')
plt.ylabel('Accuracy %')
plt.title('Classifier Accuracy TESTING subject B')
plt.show()


# In[ ]:


# print result to csv file
#  0, repeat 1, repeat 2,...
#  clasifier name A,  repeat 2

for subject in range(2):
    df_list = []
    log_repeats = log_testings[subject]
    for clf_name in log_repeats:
        df_transpose = log_repeats.get(clf_name).T
        df_list.append(df_transpose[1:2])

    dt_csv = pd.concat(df_list)
    dt_csv.to_csv(r'8Channels_Testing_NoPCA_filter_change_table_Subject{}.csv'.format(
        'AB'[subject]), index=False, header=False)


# ### Analysis of the data
#
# The following part shows how to visualize interesting information of the data.

# In[ ]:


avgs = [None, None]
channels = fv_train[0].axes[1][:5]  # show first 8 channels

channels = ['FCz', 'Cz']  # show 2 channels
nchannel = len(channels)
print(channels)

subjects = [TRAIN_A, TRAIN_B]
n_subjects = len(subjects)


fig, axes = plt.subplots(n_subjects, nchannel, sharex=True, sharey=True)
for idx, file in enumerate(subjects):
    avgs[idx] = proc.calculate_classwise_average(epo_train[idx])
#     print(avgs[idx])
    d = proc.select_channels(avgs[idx], channels)
    print(d.axes)
    for i in range(nchannel):
        axes[idx, i].plot(d.axes[-2], d.data[..., i].T)
        axes[idx, i].grid()

for i in range(nchannel):
    axes[0, i].set_title(d.axes[-1][i])

axes[1, int(nchannel/2)].set_xlabel('time [ms]')
for i in range(n_subjects):
    axes[i, 0].set_ylabel(u'amplitude [μV]')

for i in range(n_subjects):
    axes[i, nchannel-1].yaxis.set_label_position("right")
    axes[i, nchannel-1].set_ylabel('Subject %s' % 'AB'[i])

fig.legend(d.class_names, loc='upper right')
# axes[0, -1].legend(d.class_names)
plt.tight_layout()


# ## Plotting analyse data

# In[ ]:


def plot_scalps(epo, ivals):
    # ratio scalp to colorbar width
    scale = 10
    dat = proc.jumping_means(epo, ivals)
    n_classes = epo.data.shape[0]
    n_ivals = len(ivals)
    for class_idx in range(n_classes):
        vmax = np.abs(dat.data).max()
        vmax = round(vmax)
        vmin = -vmax
        for ival_idx in range(n_ivals):
            ax = plt.subplot2grid(
                (n_classes, scale*n_ivals+1), (class_idx, scale*ival_idx), colspan=scale)
            plot.ax_scalp(dat.data[class_idx, ival_idx, :],
                          epo.axes[-1], vmin=vmin, vmax=vmax)
            if class_idx == 1:
                ax.text(0, -1.5, ivals[ival_idx], horizontalalignment='center')
            if ival_idx == 0:
                ax.text(-1.5, 0, ['nontarget', 'target'][class_idx], color='bm'[
                        class_idx], rotation='vertical', verticalalignment='center')

    # colorbar
    ax = plt.subplot2grid((n_classes, scale*n_ivals+1),
                          (0, scale*n_ivals), rowspan=n_classes)
    plot.ax_colorbar(vmin, vmax, label='voltage [μV]', ticks=[vmin, 0, vmax])


# In[ ]:


JUMPING_MEANS_IVALS_A = [150, 220], [200, 260], [310, 360], [550, 660]  # 91%
JUMPING_MEANS_IVALS_B = [150, 250], [200, 280], [280, 380], [480, 610]  # 91%
for subj_idx in range(2):
    fig = plt.figure(figsize=(11, 6))
    ivals = [JUMPING_MEANS_IVALS_A, JUMPING_MEANS_IVALS_B][subj_idx]
    plot_scalps(avgs[subj_idx], ivals)
    plt.tight_layout()
    fig.subplots_adjust(left=.06, bottom=.10, right=None,
                        top=None, wspace=0, hspace=0)


# In[ ]:


fig, axes = plt.subplots(2, 1, sharex=True, sharey=True)
for i in range(2):
    r2 = proc.calculate_signed_r_square(epo_train[i])
    # switch the sign to make the plot more consistent with the timecourse. This is equivalent to reordering the classidices and calculating r2
    r2 *= -1

    max = np.max(np.abs(r2))
    im = axes[i].imshow(r2.T, aspect='auto',
                        interpolation='None', vmin=-max, vmax=max)

    axes[i].set_ylabel('%s' % (epo_train[i].names[-1]))
    axes[i].grid()
    axes[i].set_title("Subject %s" % "AB"[i])
    cb = plt.colorbar(im, ax=axes[i])
    cb.set_label('[μV]')

    axes[1].yaxis.set_major_formatter(
        ticker.IndexFormatter(epo_train[i].axes[-1]))
    mask = map(lambda x: True if x.lower().endswith(
        'z') else False, epo_train[i].axes[-1])
    axes[1].yaxis.set_major_locator(ticker.FixedLocator(np.nonzero(mask)[0]))
    axes[1].xaxis.set_major_formatter(ticker.IndexFormatter(
        ['%d' % j for j in epo_train[i].axes[-2]]))
    axes[1].xaxis.set_major_locator(ticker.MultipleLocator(6))
    axes[1].set_xlabel('%s [%s]' %
                       (epo_train[i].names[-2], epo_train[i].units[-2]))

plt.tight_layout()
plt.show()
